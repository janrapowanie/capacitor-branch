require 'json'

Pod::Spec.new do |s|
  s.name = 'CapacitorBranchDeepLinks'
  s.version = '69.4.20'
  s.summary = 'plugin'
  s.license = 'mit'
  s.homepage = 'https://google.com'
  s.author = 'janrapowanie'
  s.source = { :git => 'https://gitlab.com/janrapowanie/capacitor-branch.git', :tag => 'v69.4.20' }
  s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
  s.ios.deployment_target  = '11.0'
  s.dependency 'Capacitor'
  s.dependency 'Branch'
  s.swift_version = '5.1'
end
